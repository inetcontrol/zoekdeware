/**
 * Created by wolfgang on 24-5-16.
 */
public class Something {
    private final String name;

    public Something(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
